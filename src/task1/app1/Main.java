package task1.app1;

public class Main {
    public static void main(String[] args) {
        int gasPrice = 43; // Стоимость одного литра бензина

        if (args.length > 0) {
            int gasCount = Integer.valueOf(args[0]); // Кол-во литров
            int cost = gasPrice * gasCount; // Итоговая стоимость
            System.out.println("Стоимость " + gasCount + " л. бензина составляет " + cost + " рублей.");
        } else {
            System.out.println("Не указано количество литров!");
        }
    }
}