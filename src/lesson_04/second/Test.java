package lesson_04.second;

import lesson_04.first.Parent;
import lesson_04.first.Provider;

/**
 * Created by arty on 01.12.2018.
 */
public class Test {
    public static void main(String s[])
    {
        Provider pr = new Provider();
        Parent p = pr.getValue();
        System.out.println(p.getClass().getName());
        // (Child)p - приведет к ошибке компиляции!
    }
}
