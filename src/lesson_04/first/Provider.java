package lesson_04.first;

/**
 * Created by arty on 01.12.2018.
 */
public class Provider {
    public Parent getValue() {
        return new Child();
    }
}