package task4.app;


import task4.app.drinks.Drink;
import task4.app.drinks.HotDrink;

public class Main {

    public static void main(String[] args) {

        Drink[] hotDrinks = new HotDrink[] {new HotDrink("Чай", 150)};
        Drink[] coldDrinks = new HotDrink[] {new HotDrink("Кола", 50)};

        VendingMachine vm = new VendingMachine(hotDrinks);

        vm.addMoney(200);
        vm.giveMeADrink(0);

        vm.setDrinks(coldDrinks);
        vm.giveMeADrink(0);
    }
}