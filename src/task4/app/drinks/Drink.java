package task4.app.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}