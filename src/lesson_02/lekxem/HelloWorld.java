package lesson_02.lekxem;

/**
 * Created by arty on 01.12.2018.
 */
public class HelloWorld {
    /**
     * Основной метод, с которого начинается
     * выполнение любой Java программы.
     */
    public static void main(String args[]) {

        System.out.println("Hello, world!");
    }
}
